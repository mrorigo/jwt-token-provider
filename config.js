/**
   This file is part of jwt-token-provider.

   jwt-token-provider is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   jwt-token-provider is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with jwt-token-provider.  If not, see <http://www.gnu.org/licenses/>.
*/
var fs = require('fs');
module.exports = {
    version: JSON.parse(fs.readFileSync('./package.json')).version,
    debug: process.env.DEBUG || false,

    db: {
        mongo: {
            url: process.env.MONGO_URL || "mongodb://mongo:27017/registry"
        }
    },

    users: {
        // bcrypt salt length
        salt_length: 10
    },

    auth: {
        basic: {
            enabled: true,
            realm: 'The Realm'
        }
    },

    jwt: {
        claims: {
            // name is always added
            iat: true,
            iss: process.env.JWT_ISS || false,
            nbf: Math.floor(Date.now() - 10), // allow for 10s clock drift
            exp: Math.floor(Date.now() + (process.env.JWT_LIFE || 60000))
        },
        
        options: {
            crypto: {
                algorithm: process.env.JWT_ALGO || 'HS256',
                secret: process.env.JWT_SECRET || false
            }
        }
    },

    server: {
	ip: process.env.HTTPS_IP || '127.0.0.1',
        port: process.env.HTTPS_PORT || 443,
        name: 'jwt-token-provider',
        version: JSON.parse(fs.readFileSync('./package.json')).version,
        timeout: process.env.SERVER_TIMEOUT || 60000,
        certificate: process.env.SSL_CERTIFICATE ? fs.readFileSync(process.env.SSL_CERTIFICATE).toString() : null,
        key: process.env.SSL_KEY ? fs.readFileSync(process.env.SSL_KEY).toString() : null,
        ca: process.env.SSL_CA ? fs.readFileSync(process.env.SSL_CA).toString() : null
    },

    log: {
        name: 'jwt-token-provider',
        level: process.env.LOG_LEVEL || 'info'
    }
};

