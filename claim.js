module.exports = function(config, log) {

    var Promise = require('promise');
    var mongo = require('mongoskin');
    var db = mongo.db(config.db.mongo.url, {native_parser:true});

    db.bind('claims').bind({

        find_for_user: function(user_id) {
	    return new Promise(function(resolve, reject) {
		return this.find({user_id: user_id}).toArray(function(err, claims) {
		    if(err) {
			return reject(err);
		    }
		    resolve(claims);
		});
	    }.bind(this));
        },

        add_claim: function(user_id, service, name, value, cb) {
	    return new Promise(function(resolve, reject) {
	    
		this.insert({user_id: user_id,
                             service: service,
                             name: name,
                             value: value}, function(err, res) {
				 if(err) {
				     return reject(err);
				 }
				 resolve(res);
			     });
	    }.bind(this));
        },

        remove_claim: function(user_id, service, name) {
	    return new Promise(function(resolve, reject) {
		var q = {user_id: user_id, service: service, name: name};
		log.trace('claims.remove_claim query', q);
		this.remove(q, function(err, res) {
		    if(err) {
			return reject(err);
		    }
		    resolve(res);
		});
	    }.bind(this));
        }
    });

    return db.claims;
};
