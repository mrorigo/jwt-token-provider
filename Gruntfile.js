/**
   This file is part of jwt-token-provider.

   jwt-token-provider is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   jwt-token-provider is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with jwt-token-provider.  If not, see <http://www.gnu.org/licenses/>.
*/
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        vars: {
            deps: (Object.keys(grunt.file.readJSON('package.json').dependencies).join("/**,")+"/**").split(",")
        },
        bump: {
            options: {
                updateConfigs: ['pkg'],
                pushTo: 'origin',
                gitDescribeOptions: '--tags --always',
            }
        },
        copy: {
            build: {
                files: [
                    {expand: true, src: ['<%= pkg.files %>'], dest: 'build/', filter: 'isFile'}
                ]
            }
        },
        compress: {
            release: {
                options: {
                    archive: 'dist/<%= pkg.name%>-<%= pkg.version%>.tar.gz',
                    mode: 'tgz'
                },
                files: [
                    {expand: true, cwd: 'build/', src: ['**'], dest: '<%= pkg.name%>-<%= pkg.version%>/'},
                ]
            }
        },
        clean: ["dist", "build"],
    });

    grunt.loadNpmTasks('grunt-bump');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default', ['clean', 'copy:build', 'compress:release']);
};
