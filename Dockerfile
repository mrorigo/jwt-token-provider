FROM phusion/baseimage:latest

RUN curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
RUN sudo apt-get install -y nodejs python2.7 build-essential
RUN npm install -g bunyan node-gyp

# Clean up APT
RUN apt-get autoremove -yq

# run as our own user
RUN groupadd token && useradd -g token token

# Enable ourselves as a service
RUN mkdir /etc/service/jwt-token-provider
RUN echo "#!/bin/bash" >/etc/service/jwt-token-provider/run
RUN echo "cd /jwt-token-provider; exec /sbin/setuser token npm start" >>/etc/service/jwt-token-provider/run
RUN chmod +x /etc/service/jwt-token-provider/run

# Processing dependencies from package.json is separate step, as it does not change often
ENV PYTHON=/usr/bin/python2.7
ADD package.json /tmp/package.json
RUN npm config set loglevel info
RUN cd /tmp && npm install --production
RUN mkdir /jwt-token-provider; cp -a /tmp/node_modules /jwt-token-provider/; cp -a /tmp/package.json /jwt-token-provider/

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add main package files to /jwt-token-provider/
ADD *.js /jwt-token-provider/
ADD routes/*.js /jwt-token-provider/routes/
ADD cli  /
RUN chmod +x  /cli
ADD Dockerfile /

# Make sure services start
CMD ["/sbin/my_init"]
