module.exports = function(config) {

    var bcrypt = require('bcrypt');
    var Promise = require('promise');
    var mongo = require('mongoskin');
    var db = mongo.db(config.db.mongo.url, {native_parser:true});

    db.bind('users').bind({
        add: function(name, password) {
	    return new Promise(function(resolve, reject) {
		bcrypt.hash(password, config.users.salt_length, function(err, hash) {
                    this.insert({name: name, password: hash}, function(err, res) {
			if(err) {
                            return reject(err);
			}
			resolve(res);
                    });
		}.bind(this));
	    }.bind(this));
        },

        list: function() {
	    return new Promise(function(resolve, reject) {
		this.find({}).toArray(function(err, list) {
		    if(err) {
			return reject(err);
		    }
		    resolve(list);
		})
	    }.bind(this));
        },

        remove_by_name: function(name) {
	    console.log('remove ', name);
	    return new Promise(function(resolve, reject) {
		console.log('remove2 ', name);
		this.remove({name: name}, {justOne: true}, function(err, res) {
		    console.log('removed ', arguments);
		    if(err) {
			return reject(err);
		    }
		    resolve(res);
		});
	    }.bind(this));
        },

        find_by_name: function(name) {
	    return new Promise(function(resolve, reject) {
		this.findOne({name: name}, function(err, user) {
		    if(err) {
			return reject(err);
		    }
		    resolve(user);
		});
	    }.bind(this));
        },

        set_password: function(name, password) {
	    return new Promise(function(resolve, reject) {
		this.find_by_name(name)
		    .then(function(user) {
			if(!user) {
			    return reject(new Error('user not found'));
			}
			
			bcrypt.hash(password, config.users.salt_length, function(err, hash) {
			    console.log('hashed pw', err, hash);
			    this.update({_id: user._id}, {$set: {password: hash}}, function(err, res) {
				if(err) {
				    return reject(err);
				}
				resolve(true);
			    });
			}.bind(this));
		    }.bind(this))
		    .catch(function(err) {
			reject(err);
		    });
	    }.bind(this));
        },
        
        verify_password: function(name, password) {
	    return new Promise(function(resolve, reject) {
		this.find_by_name(name).then(function(user)  {
                    if(!user) {
			return reject(new Error('no such user'));
                    }

                    bcrypt.compare(password, user.password, function(err, res) {
			if(err) {
                            return reject(new Error('bcrypt.compare error', err));
			}
			if(res !== true) {
			    reject(new Error('Invalid password'));
			}
			return resolve(user);
                    });
		}).catch(function(err) {
		    reject(new Error('error in verify_password/find_by_name', err));
		});
	    }.bind(this));
	}
    });
    db.users.ensureIndex( { "name": "text" }, {unique: true} );

    return db.users;
};
