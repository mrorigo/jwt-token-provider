/**
   This file is part of jwt-token-provider.

   jwt-token-provider is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   jwt-token-provider is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with jwt-token-provider.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function(server, config, log) {
    var JWT = require('jwt-async');
    var jwt = new JWT(config.jwt.options);

    var users = require('../user')(config,log);
    var claims = require('../claim')(config,log);

    var basic_auth_enabled = config.auth.basic.enabled || true;
    var basic_auth_realm = config.auth.basic.realm || true;

    function require_auth(res) {
        if(basic_auth_enabled)
            res.setHeader('www-authenticate', 'Basic realm="'+basic_auth_realm+'"');
        // TODO: digest?
        res.send(401, 'Please provide authentication credentials');
    }

    server.get('/token', function(req, res, next) {
        var service = req.query.service;
        if(!service) {
            res.send(400, 'Must provide service');
            return next();
        }
        var scope = req.query.scope || ''; // currently ignored
	
        if(!req.authorization || !req.authorization.basic) {
            require_auth(res);
            return next();
        }

        var username = req.authorization.basic.username;
        var password = req.authorization.basic.password;
        log.info('username,password', [username,password]);
        users.verify_password(username, password).then(function(user) {
            if(!user) {
                log.warn('get.token:auth failed', username);
                require_auth(res);
                return next();
            }
	    log.info('authed user', user);

            // authed user!
            claims.find_for_user(user._id).then(function(user_claims) {
		log.info('user claims', user_claims);
                var claims = {};
                claims.name = user.name;
                user_claims
                    .filter(function _service_filter(uc) {
                        return uc.service == '*' || uc.service === service;
                    })
                    .filter(function(uc) { claims[uc.name] = uc.value; });
                log.trace('claims', claims);
		if(claims.length === 0)
		    claims = null;
		    
                jwt.sign(claims, function(err, data) {
                    if(err)
                        return next(new Error('jwt sign error: ' + err));
                    log.trace('signed', err, data);
                    res.json(200, {token: data});
                    return next();
                });
            })
		.catch(function(err) {
                    return next(new Error('Internal Error', err));
		});
        }).catch(function(err) {
            require_auth(res);
	    return next();
	});

    });
    

};
