# JWT token provider

Simplistic mongodb-backed JWT token provider with SSL support that handles users and claims. Given proper
credentials, it returns a JWT token with per-user defined claims.

It includes both HTTP API for generating tokens, a command line util for managing users and claims,
and a Docker image build file for easy cloud-deployment.


## Quickstart

### Try the prebuilt Docker image

https://hub.docker.com/r/allbinarian/jwt-token-provider/


### From source

#### Clone the repository

    git clone https://bitbucket.org/mrorigo/jwt-token-provider.git

#### Run from the shell

    npm install
    export MONGO_URL=mongodb://127.0.0.1/jwt-provider
    export HTTPS_PORT=1443
    export JWT_SECRET=secret
    export JWT_ISS=test-iss
    node cli.js --add-user user1 password
    node cli.js --add-claim user1 admin true
    npm start


#### Run in a Docker container

    docker build -t jwt-token-provider .
    CMD="docker run \
      -e MONGO_URL=mongodb://127.0.0.1/jwt-provider \
      -e HTTPS_PORT=1443 \
      -e JWT_SECRET=secret \
      -e JWT_ISS=test-iss \
      -p 1443:1443 \
      -it --rm jwt-token-provider"
    $CMD /cli --add-user user1 password
    $CMD /cli --add-claim user1 admin true

### Try it out

Use curl to fetch a new token for the user created above.
    
    curl -u user1:password https://127.0.0.1:1433/token
    {"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJuYW1lIjoidXNlcjEiLCJhZG1pbiI6InRydWUifQ.6dyQjCVb7lYCOf6bj57n1wqT7GK8XlttNquwESEzd56ioXXjTPuYnDB7COTMaTg5UgpBcUpPeXDjEcWURzCM2Q"}

Paste this token into jwt.io along with the secret above (secret), and verify the token!